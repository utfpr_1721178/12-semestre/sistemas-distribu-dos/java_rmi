package disciplina.sistemas.distribuidos.common.models;

import java.io.Serializable;
import java.util.Objects;

public class Stock implements Serializable {
	private String companyName;
	private String stockCode;
	private String sector;

	public Stock(String companyName, String stockCode, String sector) {
		this.companyName = companyName;
		this.stockCode = stockCode;
		this.sector = sector;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getStockCode() {
		return stockCode;
	}

	public void setStockCode(String stockCode) {
		this.stockCode = stockCode;
	}

	public String getSector() {
		return sector;
	}

	public void setSector(String sector) {
		this.sector = sector;
	}

	@Override
	public boolean equals(Object o) {
		if (!(o instanceof Stock)) return false;
		Stock stock = (Stock) o;
		return getStockCode().equals(stock.getStockCode());
	}

	@Override
	public int hashCode() {
		return Objects.hash(getStockCode());
	}
}

package disciplina.sistemas.distribuidos.common.models;

import disciplina.sistemas.distribuidos.common.utils.SingletonTools;

public class Quote {

	private float price;

	private final Stock stock;

	public Quote(float price, Stock stock) {
		this.price = price;
		this.stock = stock;
	}

	public synchronized float getPrice() {
		this.price += SingletonTools.getRandomGenerator().nextFloat() % 10;
		return  this.price;
	}

	public synchronized void setPrice(float price) {
		this.price = price;
	}

	public Stock getStock() {
		return stock;
	}
}

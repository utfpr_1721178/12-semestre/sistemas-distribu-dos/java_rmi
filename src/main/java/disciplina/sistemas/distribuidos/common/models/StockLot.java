package disciplina.sistemas.distribuidos.common.models;

public class StockLot {

	private final Stock stock;

	private Integer sizeOfLot;

	public StockLot(Stock stock, Integer sizeOfLot) {
		this.stock = stock;
		this.sizeOfLot = sizeOfLot;
	}

	public Stock getStock() {
		return stock;
	}

	public Integer getSizeOfLot() {
		return sizeOfLot;
	}

	public void setSizeOfLot(Integer sizeOfLot) {
		this.sizeOfLot = sizeOfLot;
	}
}

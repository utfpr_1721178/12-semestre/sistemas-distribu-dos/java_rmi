package disciplina.sistemas.distribuidos.common.utils;

import org.jetbrains.annotations.NotNull;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;

public final class OutputTools {

	private static final short WIDTH_OF_LINE = 99;
	private static final short UNUSABLE_SPACE_ON_LINE = 4;

	private OutputTools() {
		throw new IllegalStateException("Utility class");
	}

	public static void drawLine() {
		StringBuilder line = new StringBuilder();
		for (int index = 0; index < WIDTH_OF_LINE; index++) {
			line.append("*");
		}
		showNewLine(line.toString());
	}

	public static void showMessage(@NotNull String message) {
		StringBuilder textToBeWrittenOnASingleLine = new StringBuilder();

		if (message.length() <= WIDTH_OF_LINE - UNUSABLE_SPACE_ON_LINE) {
			textToBeWrittenOnASingleLine.append("* ").append(message).append(completeEmptySpaces(message.length())).append(" *");
			showNewLine(textToBeWrittenOnASingleLine.toString());
			return;
		}

		Queue<String> stringQueue = new LinkedList<>(Arrays.asList(message.split(" ")));

		do {
			textToBeWrittenOnASingleLine.append("* ");

			textToBeWrittenOnASingleLine.append(stringQueue.remove());
			while (!stringQueue.isEmpty() &&
					textToBeWrittenOnASingleLine.length() + stringQueue.peek().length() <= WIDTH_OF_LINE - UNUSABLE_SPACE_ON_LINE
			) {
				textToBeWrittenOnASingleLine.append(" ").append(stringQueue.remove());
			}

			textToBeWrittenOnASingleLine.append(completeEmptySpaces(textToBeWrittenOnASingleLine.length())).append(" *");

			showNewLine(textToBeWrittenOnASingleLine.toString());
			textToBeWrittenOnASingleLine = new StringBuilder();
		} while (!stringQueue.isEmpty());

	}

	public static void showCentralizedMessage(@NotNull String message) {
		if (message.length() > WIDTH_OF_LINE - UNUSABLE_SPACE_ON_LINE) {
			OutputTools.showMessage(message);
		} else {

			int totalEmptySpace = (WIDTH_OF_LINE - UNUSABLE_SPACE_ON_LINE) - message.length();
			int leftEmptySpace = totalEmptySpace / 2;
			Integer rightEmptySpace = totalEmptySpace - leftEmptySpace;

			String stringBuilder = "* " +
					repeatSpaces(
							leftEmptySpace
					) +
					message +
					repeatSpaces(
							rightEmptySpace
					) +
					" *";

			showNewLine(stringBuilder);
		}
	}

	private static void showNewLine(String message) {
		System.out.println(message);
	}

	private static String repeatSpaces(@NotNull Integer numberOfSpaces) {
		StringBuilder spaces = new StringBuilder();

		for (int i = 0; i < numberOfSpaces; i++) {
			spaces.append(" ");
		}

		return spaces.toString();
	}

	private static String completeEmptySpaces(@NotNull Integer sizeOfMessage) {
		StringBuilder spaces = new StringBuilder();

		for (int i = 0; i < WIDTH_OF_LINE - UNUSABLE_SPACE_ON_LINE - sizeOfMessage; i++) {
			spaces.append(" ");
		}

		return spaces.toString();
	}

	public static void drawOptions(String[] options, boolean selectable) {
		if (selectable) {
			drawSelectableOptions(options);
		} else {
			drawNotSelectableOptions(options);
		}

		OutputTools.drawLine();
	}

	public static void killingServerByInternalError(String errorMessage) {
		System.out.println("\n\n\n\n");

		OutputTools.drawLine();
		OutputTools.showCentralizedMessage(errorMessage);
		OutputTools.drawLine();

		System.exit(1);
	}

	private static void drawSelectableOptions(@NotNull String[] options) {
		for (int index = 0; index < options.length; index++) {

			OutputTools.showMessage(index + " -> " + options[index]);

		}
	}

	private static void drawNotSelectableOptions(@NotNull String[] options) {
		for (String option : options) {

			OutputTools.showMessage(" > " + option);

		}
	}

}
package disciplina.sistemas.distribuidos.common.utils;

import java.util.Random;

public class SingletonTools {

	private static final Random randomGenerator = new Random();

	private SingletonTools(){
		throw new IllegalStateException("Utility class");
	}

	public static Random getRandomGenerator() {
		return randomGenerator;
	}
}

package disciplina.sistemas.distribuidos.common.utils;

import java.util.Scanner;

public class TerminalInput {

	private static Scanner scanner;

	private TerminalInput() {
		throw new IllegalStateException("Utility class");
	}

	private static Scanner getScanner() {
		if (scanner == null) {
			scanner = new Scanner(System.in);
		}
		return scanner;
	}

	public static String getNextLine() {
		return getScanner().nextLine();
	}
}

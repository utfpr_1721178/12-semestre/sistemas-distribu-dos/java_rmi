package disciplina.sistemas.distribuidos.common.utils;

import disciplina.sistemas.distribuidos.common.models.Stock;

import java.util.ArrayList;
import java.util.List;

public final class GlobalInformation {

	public static final String SERVER_NAME = "JAVA_RMI_SERVER";

	public static final Integer DNS_SERVICE_PORT = 2020;

	public static final String DNS_SERVICE_ADDRESS = "localhost";

	public static final List<Stock> STOCKS = new ArrayList<>();

	public static final List<String> SECTORS = new ArrayList<>();

	private GlobalInformation() {
		throw new IllegalStateException("Utility class");
	}

	static {
		STOCKS.add(new Stock("AMBEV S/A", "ABEV3", "Consumo não-cíclico"));
		STOCKS.add(new Stock("AZUL", "AZUL4", "Bens Industriais"));
		STOCKS.add(new Stock("B2W DIGITAL", "BTOW3", "Comércio"));
		STOCKS.add(new Stock("B3", "B3SA3", "Financeiro"));
		STOCKS.add(new Stock("BB SEGURIDADE", "BBSE3", "Seguros"));
		STOCKS.add(new Stock("BR MALLS PAR", "VRML3", "Exploração de imóveis"));
		STOCKS.add(new Stock("BRADESCO", "BBDC4", "Financeiro"));
		STOCKS.add(new Stock("BRADESCO", "BBDC3", "Financeiro"));
		STOCKS.add(new Stock("BRADESPAR", "BRAP4", "Mineração"));
		STOCKS.add(new Stock("BANCO DO BRASIL", "BBAS3", "Financeiro"));
		STOCKS.add(new Stock("BRASKEM", "BRKM5", "Químicos"));
		STOCKS.add(new Stock("BRF S/A", "BRFS3", "Alimentos processados"));
		STOCKS.add(new Stock("CCR S/A", "CCRO3", "Bens industriais"));
		STOCKS.add(new Stock("CEMIG", "CMIG4", "Energia elétrica"));
		STOCKS.add(new Stock("CIELO", "CIEL3", "Financeiro"));
		STOCKS.add(new Stock("COSAN", "CSAN3", "Petróleo, Gás e Biocombustíveis"));
		STOCKS.add(new Stock("CVC BRASIL", "CVCB3", "Viagens e lazer"));
		STOCKS.add(new Stock("CYRELA REALT", "CYRE3", "Construção civil"));
		STOCKS.add(new Stock("ECORODOVIAS", "ECOR3", "Bens industriais"));
		STOCKS.add(new Stock("ELETROBRAS", "ELET3", "Energia elétrica"));
		STOCKS.add(new Stock("ELETROBRAS", "ELET6", "Energia Elétrica"));
		STOCKS.add(new Stock("EMBRAER", "EMBR3", "Bens industriais"));
		STOCKS.add(new Stock("ENERGIAS BR", "ENBR3", "Energia elétrica"));
		STOCKS.add(new Stock("ENGIE BRASIL", "ENGIE3", "Energia elétrica"));
		STOCKS.add(new Stock("EQUATORIAL", "EQTL3", "Energia elétrica"));
		STOCKS.add(new Stock("ESTACIO PART", "YDUQ3", "Educacionais"));
		STOCKS.add(new Stock("FLEURY", "FLRY3", "Saúde"));
		STOCKS.add(new Stock("GERDAU", "GGBR4", "Siderurgia e Metalurgia"));
		STOCKS.add(new Stock("GERDAU MET", "GOAU4", "Siderurgia e Metalurgia"));
		STOCKS.add(new Stock("GOL", "GOLL4", "Bens industriais"));
		STOCKS.add(new Stock("HYPERA", "HYPE3", "Saúde"));
		STOCKS.add(new Stock("IGUATEMI", "IGTA3", "Exploração de imóveis"));
		STOCKS.add(new Stock("IRB BRASIL RE", "IRBR3", "Seguros"));
		STOCKS.add(new Stock("ITAUSA", "ITSA4", "Financeiro"));
		STOCKS.add(new Stock("ITAU UNIBANCO", "ITUB4", "Financeiro"));
		STOCKS.add(new Stock("JBS", "JBSS3", "Alimentos processados"));
		STOCKS.add(new Stock("KLABIN S/A", "KLBN11", "Madeira e papel"));
		STOCKS.add(new Stock("KROTON", "KROT3", "Educacionais"));
		STOCKS.add(new Stock("LOCALIZA", "RENT3", "Locação de veículos"));
		STOCKS.add(new Stock("LOJAS AMERICANAS", "LAME4", "Comércio"));
		STOCKS.add(new Stock("LOJAS RENNER", "LREN3", "Comércio"));
		STOCKS.add(new Stock("MAGAZINE LUIZA", "MGLU3", "Comércio"));
		STOCKS.add(new Stock("MARFRIG", "MRFG3", "Alimentos processados"));
		STOCKS.add(new Stock("MRV", "MRVE3", "Construção civil"));
		STOCKS.add(new Stock("MULTIPLAN", "MULT3", "Exploração de imóveis"));
		STOCKS.add(new Stock("NATURA", "NATU3", "Consumo não-cíclico"));
		STOCKS.add(new Stock("PÃO DE AÇÚCAR - CBD", "PCAR4", "Consumo não-cíclico"));
		STOCKS.add(new Stock("PETROBRAS", "PETR4", "Petróleo, Gás e Biocombustíveis"));
		STOCKS.add(new Stock("PETROBRAS", "PETR3", "Petróleo, Gás e Biocombustíveis"));
		STOCKS.add(new Stock("PETROBRAS BR", "BRDT3", "Petróleo, Gás e Biocombustíveis"));
		STOCKS.add(new Stock("QUALICORP", "QUAL3", "Saúde"));
		STOCKS.add(new Stock("RAIADROGASIL", "RADL3", "Saúde"));
		STOCKS.add(new Stock("RUMO S/A", "RAIL3", "Bens industriais"));
		STOCKS.add(new Stock("SABESP", "SBSP3", "Saneamento"));
		STOCKS.add(new Stock("SANTANDER BR", "SANB11", "Financeiro"));
		STOCKS.add(new Stock("SID NACIONAL", "CSNA3", "Siderurgia e Metalurgia"));
		STOCKS.add(new Stock("SMILES", "SMLS3", "Diversos"));
		STOCKS.add(new Stock("SUZANO S/A", "SUZB5", "Madeira e papel"));
		STOCKS.add(new Stock("TAESA", "TAEE11", "Energia elétrica"));
		STOCKS.add(new Stock("TELEF BRASIL", "VIVT4", "Telecomunicações"));
		STOCKS.add(new Stock("TIM PART S/A", "TIMP3", "Telecomunicações"));
		STOCKS.add(new Stock("ULTRAPAR", "UGPA3", "Petróleo, Gás e Biocombustíveis"));
		STOCKS.add(new Stock("USIMINAS", "USIM5", "Siderurgia e Metalurgia"));
		STOCKS.add(new Stock("VALE", "VALE3", "Mineração"));
		STOCKS.add(new Stock("VIA VAREJO", "VVAR3", "Comércio"));
		STOCKS.add(new Stock("WEG", "WEGE3", "Bens Industriais"));

		for(Stock stock: STOCKS  ) {
			if(!SECTORS.contains( stock.getStockCode() )) {
				SECTORS.add(stock.getStockCode());
			}
		}
	}
}
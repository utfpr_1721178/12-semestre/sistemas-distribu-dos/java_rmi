package disciplina.sistemas.distribuidos.common.interfaces;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.UUID;

public interface ClientInterfaceAPI extends Remote {

	void notifyEvent(String notify) throws RemoteException;

	void killHomeBroker() throws RemoteException;

	UUID getUniversallyUniqueIdentifier() throws RemoteException;
}

package disciplina.sistemas.distribuidos.common.interfaces;

import disciplina.sistemas.distribuidos.common.models.Quote;
import disciplina.sistemas.distribuidos.common.models.Stock;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.Date;
import java.util.List;

public interface ServerInterfaceAPI extends Remote {

	void registryClient(ClientInterfaceAPI clientInterfaceAPI) throws RemoteException;

	List<Quote> getQuotes(ClientInterfaceAPI clientInterfaceAPI) throws RemoteException;

	List<Stock> getInterestStocks(ClientInterfaceAPI clientInterfaceAPI) throws RemoteException;

	void addStockToInterestList(Stock stock, ClientInterfaceAPI clientInterfaceAPI) throws RemoteException;

	void removeStockToInterestList(Stock stock, ClientInterfaceAPI clientInterfaceAPI) throws RemoteException;

	void buyStock(Stock stock, Integer amount, Float maximumPrice, Date deadLine, ClientInterfaceAPI clientInterfaceAPI) throws RemoteException;

	void sellStock(Stock stock, Integer amount, Float minimumPrice, Date deadLine, ClientInterfaceAPI clientInterfaceAPI) throws RemoteException;
}

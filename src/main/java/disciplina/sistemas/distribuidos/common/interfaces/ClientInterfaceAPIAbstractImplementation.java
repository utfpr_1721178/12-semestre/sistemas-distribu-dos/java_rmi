package disciplina.sistemas.distribuidos.common.interfaces;

import java.io.Serializable;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.Objects;
import java.util.UUID;

public abstract class ClientInterfaceAPIAbstractImplementation extends UnicastRemoteObject implements ClientInterfaceAPI, Serializable {

	private final UUID universallyUniqueIdentifier;

	protected ClientInterfaceAPIAbstractImplementation() throws RemoteException {
		super();
		universallyUniqueIdentifier = UUID.randomUUID();
	}

	public UUID getUniversallyUniqueIdentifier() {
		return universallyUniqueIdentifier;
	}

	@Override
	public boolean equals(Object o) {
		if (!(o instanceof ClientInterfaceAPIAbstractImplementation)) return false;
		if (!super.equals(o)) return false;
		ClientInterfaceAPIAbstractImplementation that = (ClientInterfaceAPIAbstractImplementation) o;
		return universallyUniqueIdentifier.equals(that.getUniversallyUniqueIdentifier());
	}

	@Override
	public int hashCode() {
		return Objects.hash(super.hashCode(), universallyUniqueIdentifier);
	}
}

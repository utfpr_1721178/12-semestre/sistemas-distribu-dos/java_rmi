package disciplina.sistemas.distribuidos;

import disciplina.sistemas.distribuidos.server.controller.ServerController;
import disciplina.sistemas.distribuidos.server.models.enums.ServerMenuTypes;
import disciplina.sistemas.distribuidos.server.util.ServerControllerSingleton;

public class MainOfServerService {

	public static void main(String[] args) {
		ServerController controller = ServerControllerSingleton.getCONTROLLER();

		controller.alterCurrentMenu(ServerMenuTypes.MAIN);
	}

}



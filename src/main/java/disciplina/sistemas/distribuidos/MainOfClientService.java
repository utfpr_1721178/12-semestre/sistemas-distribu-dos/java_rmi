package disciplina.sistemas.distribuidos;

import disciplina.sistemas.distribuidos.client.controller.ClientController;
import disciplina.sistemas.distribuidos.client.model.enums.ClientMenuTypes;
import disciplina.sistemas.distribuidos.client.utils.ClientControllerSingleton;

public class MainOfClientService {

	public static void main(String[] args) {

		ClientController controller = ClientControllerSingleton.getCONTROLLER();

		controller.alterCurrentMenu(ClientMenuTypes.MAIN);
	}

}

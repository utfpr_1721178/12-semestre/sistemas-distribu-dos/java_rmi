package disciplina.sistemas.distribuidos.client.model.enums;

public enum ClientMainMenuOptions {
	QUOTES('0'),
	ADD_STOCK_TO_INTEREST_LIST('1'),
	REMOVE_STOCK_TO_INTEREST_LIST('2'),
	INTEREST_STOCK_LIST('3'),
	LOG_LIST('4'),
	STOP('5');

	private final char option;

	ClientMainMenuOptions(char option) {
		this.option = option;
	}

	public char getOption() {
		return option;
	}
}

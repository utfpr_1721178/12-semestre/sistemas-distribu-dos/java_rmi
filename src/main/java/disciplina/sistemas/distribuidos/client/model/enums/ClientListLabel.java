package disciplina.sistemas.distribuidos.client.model.enums;

public enum ClientListLabel {
	MAIN("Return main menu");

	private final String label;

	ClientListLabel(String label) {
		this.label = label;
	}

	public String getLabel() {
		return label;
	}
}

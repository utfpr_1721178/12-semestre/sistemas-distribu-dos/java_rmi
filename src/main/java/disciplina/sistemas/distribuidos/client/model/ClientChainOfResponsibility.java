package disciplina.sistemas.distribuidos.client.model;

public interface ClientChainOfResponsibility {

	void receiveProblem(ClientState state);

	void passesTheProblemOnToTheNextChainLink(ClientState state);

	Boolean tryHandleWithProblem(ClientState state);

	void addChainLinkToChain(ClientChainOfResponsibility chainLink);
}

package disciplina.sistemas.distribuidos.client.model.enums;

public enum ClientMainMenuLabels {
	QUOTES("Get quotes"),
	ADD_STOCK_TO_INTEREST_LIST("Add stock to interest list"),
	REMOVE_STOCK_TO_INTEREST_LIST("Remove stock to interest list"),
	INTEREST_STOCK_LIST("Interest stock list"),
	LOG("See process log"),
	STOP("Stop server");

	private final String label;

	ClientMainMenuLabels(String label) {
		this.label = label;
	}

	public String getLabel() {
		return label;
	}
}

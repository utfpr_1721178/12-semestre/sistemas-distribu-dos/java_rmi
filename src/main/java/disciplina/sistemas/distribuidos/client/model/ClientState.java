package disciplina.sistemas.distribuidos.client.model;

import disciplina.sistemas.distribuidos.client.model.enums.ClientMenuTypes;
import disciplina.sistemas.distribuidos.client.model.enums.ClientStateAlterationsTypes;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class ClientState {

	private final List<String> log;

	private ClientMenuTypes currentMenu;

	private final Queue<ClientStateAlterationsTypes> lastAlterations;

	private String lastUserInput;

	public ClientState() {

		this.log = new ArrayList<>();

		this.currentMenu = null;

		this.lastAlterations = new LinkedList<>();

		this.lastUserInput = "";
	}

	public void addLog(String message) {
		synchronized (this.log) {
			this.log.add(message);
		}
		synchronized (this.lastAlterations) {
			this.lastAlterations.add(ClientStateAlterationsTypes.NEW_LOG);
		}
	}

	public List<String> getLog() {
		synchronized (this.log) {
			return log;
		}
	}

	public synchronized ClientMenuTypes getCurrentMenu() {
		return currentMenu;
	}

	public synchronized void setCurrentMenu(ClientMenuTypes currentMenu) {
		this.currentMenu = currentMenu;
		synchronized (this.lastAlterations){
			this.lastAlterations.add(ClientStateAlterationsTypes.MENU_CHANGE);
		}
	}

	public synchronized String getLastUserInput() {
		return lastUserInput;
	}

	public synchronized void setLastUserInput(String lastUserInput, ClientStateAlterationsTypes inputType) {
		this.lastUserInput = lastUserInput;
		synchronized (this.lastAlterations) {
			this.lastAlterations.add(inputType);
		}
	}

	public ClientStateAlterationsTypes getLastStateAlteration() {
		return lastAlterations.peek();
	}

	public synchronized void clearLastChange() {
		this.lastUserInput = "";
		synchronized (this.lastAlterations) {
			this.lastAlterations.remove();
		}
	}

	public Boolean isDirty() {
		synchronized (this.lastAlterations){
			return !this.lastAlterations.isEmpty();
		}
	}
}

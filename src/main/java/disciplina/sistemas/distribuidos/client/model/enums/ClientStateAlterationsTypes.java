package disciplina.sistemas.distribuidos.client.model.enums;

public enum ClientStateAlterationsTypes {
	USER_OPTION_INPUT, MENU_CHANGE, NEW_LOG, USER_VALUE_INPUT
}

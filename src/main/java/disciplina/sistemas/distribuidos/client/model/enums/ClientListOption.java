package disciplina.sistemas.distribuidos.client.model.enums;

public enum ClientListOption {
	MAIN('0');

	private final char option;

	ClientListOption(char option) {
		this.option = option;
	}

	public char getOption() {
		return option;
	}
}

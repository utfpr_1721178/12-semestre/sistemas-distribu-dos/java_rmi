package disciplina.sistemas.distribuidos.client.model.enums;

public enum ClientMenuTypes {
	MAIN, LOGS_LIST, QUOTES_LIST, ADD_STOCK_TO_INTEREST_LIST, REMOVE_STOCK_TO_INTEREST_LIST, INTEREST_LIST
}

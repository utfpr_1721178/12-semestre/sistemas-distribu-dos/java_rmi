package disciplina.sistemas.distribuidos.client.model;

public abstract class ClientChainOfResponsibilityAbstractImplementation implements ClientChainOfResponsibility {

	private ClientChainOfResponsibility nextChain;

	public ClientChainOfResponsibilityAbstractImplementation(ClientChainOfResponsibility nextChain) {
		this.nextChain = nextChain;
	}

	@Override
	public void passesTheProblemOnToTheNextChainLink(ClientState state) {
		if (this.nextChain != null) {
			this.nextChain.receiveProblem(state);
		} else {
			state.clearLastChange();
		}
	}

	@Override
	public void addChainLinkToChain(ClientChainOfResponsibility chainLink) {
		if (this.nextChain == null) {
			this.nextChain = chainLink;
		} else {
			this.nextChain.addChainLinkToChain(chainLink);
		}
	}

}

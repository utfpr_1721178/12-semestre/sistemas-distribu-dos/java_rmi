package disciplina.sistemas.distribuidos.client.utils;

import disciplina.sistemas.distribuidos.common.interfaces.ClientInterfaceAPIAbstractImplementation;

import java.rmi.RemoteException;

public class ClientImplementationAPI extends ClientInterfaceAPIAbstractImplementation {

	public ClientImplementationAPI() throws RemoteException {
		super();
	}

	@Override
	public void killHomeBroker() throws RemoteException {
		ClientControllerSingleton.getCONTROLLER().closeTheApplication();
	}

	@Override
	public void notifyEvent(String notify) throws RemoteException {
		ClientControllerSingleton.getCONTROLLER().createNewLog(notify);
	}

}

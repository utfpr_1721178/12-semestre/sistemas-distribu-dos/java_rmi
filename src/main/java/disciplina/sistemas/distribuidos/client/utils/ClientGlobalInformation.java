package disciplina.sistemas.distribuidos.client.utils;

public class ClientGlobalInformation {

	public static final String[] MAIN_MENU = {
			"Get quotes",
			"See process log",
			"Stop server"
	};

	public static final String[] LISTS = {
			"Return to main menu"
	};

	public static final Integer MAXIMUM_NUMBER_OF_VISIBLE_RECORDS = 10;

	private ClientGlobalInformation() {
		throw new IllegalStateException("Utility class");
	}
}

package disciplina.sistemas.distribuidos.client.utils;

import disciplina.sistemas.distribuidos.client.controller.ClientController;

public class ClientControllerSingleton {

	private static final ClientController CONTROLLER = new ClientController();

	private ClientControllerSingleton() {
		throw new IllegalStateException("Utility class");
	}

	public static ClientController getCONTROLLER() {
		return CONTROLLER;
	}
}

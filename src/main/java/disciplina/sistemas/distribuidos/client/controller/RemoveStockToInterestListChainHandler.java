package disciplina.sistemas.distribuidos.client.controller;

import disciplina.sistemas.distribuidos.client.model.ClientChainOfResponsibility;
import disciplina.sistemas.distribuidos.client.model.ClientChainOfResponsibilityAbstractImplementation;
import disciplina.sistemas.distribuidos.client.model.ClientState;
import disciplina.sistemas.distribuidos.client.model.enums.ClientMenuTypes;
import disciplina.sistemas.distribuidos.client.utils.ClientControllerSingleton;
import disciplina.sistemas.distribuidos.common.utils.GlobalInformation;
import disciplina.sistemas.distribuidos.common.utils.OutputTools;
import disciplina.sistemas.distribuidos.common.utils.TerminalInput;

public class RemoveStockToInterestListChainHandler extends ClientChainOfResponsibilityAbstractImplementation {

	public RemoveStockToInterestListChainHandler(ClientChainOfResponsibility nextChain) {
		super(nextChain);
	}

	@Override
	public void
	receiveProblem(ClientState state) {

		boolean areTrySuccessful = false;

		if (state.getCurrentMenu() == ClientMenuTypes.REMOVE_STOCK_TO_INTEREST_LIST) {
			areTrySuccessful = this.tryHandleWithProblem(state);
		}

		if (Boolean.FALSE.equals(areTrySuccessful)) {
			this.passesTheProblemOnToTheNextChainLink(state);
		}
	}

	@Override
	public Boolean tryHandleWithProblem(ClientState state) {

		boolean successful;

		switch (state.getLastStateAlteration()) {
			case MENU_CHANGE:
				this.showMainMenu();
				state.clearLastChange();
				ClientControllerSingleton.getCONTROLLER().handlerUserOptionInput(TerminalInput.getNextLine());
				successful = true;
				break;
			case USER_OPTION_INPUT:
				String userInput = state.getLastUserInput();
				state.clearLastChange();
				this.handleWithUserInput(userInput);
				successful = true;
				break;
			default:
				successful = false;
		}

		return successful;

	}

	private void handleWithUserInput(String userInput) {
		boolean isNumber = true;

		try {
			this.handleWithNumericSelectedOption(Integer.parseInt(userInput));

		} catch (NumberFormatException e) {
			isNumber = false;
		}

		if (Boolean.FALSE.equals(isNumber)) {
			this.showMainMenu();
			ClientControllerSingleton.getCONTROLLER().handlerUserOptionInput(TerminalInput.getNextLine());
		}
	}

	private void handleWithNumericSelectedOption(int selectedOption) {

		if (selectedOption == 0) {
			ClientControllerSingleton.getCONTROLLER().alterCurrentMenu(ClientMenuTypes.MAIN);
		} else {
			try {
				ClientControllerSingleton.getCONTROLLER().removeStockToInterestList(
						GlobalInformation.STOCKS.get(selectedOption - 1)
				);
			} catch (IndexOutOfBoundsException e) {
				this.showMainMenu();
				ClientControllerSingleton.getCONTROLLER().handlerUserOptionInput(TerminalInput.getNextLine());
			}
		}
	}

	private void showMainMenu() {

		String[] optionsLabel = new String[GlobalInformation.STOCKS.size() + 1];

		optionsLabel[0] = "Return to main menu";

		for (int index = 0; index < GlobalInformation.STOCKS.size(); index++) {
			optionsLabel[index + 1] = GlobalInformation.STOCKS.get(index).getStockCode() +
					" - " +
					GlobalInformation.STOCKS.get(index).getCompanyName();
		}

		System.out.println("\n\n\n");
		OutputTools.drawLine();
		OutputTools.drawOptions(optionsLabel, true);
		OutputTools.showCentralizedMessage("Client: select stock to remove from interest list");
		OutputTools.drawLine();
	}
}

package disciplina.sistemas.distribuidos.client.controller;

import disciplina.sistemas.distribuidos.client.model.ClientChainOfResponsibility;
import disciplina.sistemas.distribuidos.client.model.ClientChainOfResponsibilityAbstractImplementation;
import disciplina.sistemas.distribuidos.client.model.ClientState;
import disciplina.sistemas.distribuidos.client.model.enums.ClientMainMenuLabels;
import disciplina.sistemas.distribuidos.client.model.enums.ClientMainMenuOptions;
import disciplina.sistemas.distribuidos.client.model.enums.ClientMenuTypes;
import disciplina.sistemas.distribuidos.client.utils.ClientControllerSingleton;
import disciplina.sistemas.distribuidos.common.utils.OutputTools;
import disciplina.sistemas.distribuidos.common.utils.TerminalInput;

public class MainMenuClientChainHandler extends ClientChainOfResponsibilityAbstractImplementation {

	public MainMenuClientChainHandler(ClientChainOfResponsibility nextChain) {
		super(nextChain);
	}

	@Override
	public void
	receiveProblem(ClientState state) {

		boolean areTrySuccessful = false;

		if (state.getCurrentMenu() == ClientMenuTypes.MAIN) {
			areTrySuccessful = this.tryHandleWithProblem(state);
		}

		if (Boolean.FALSE.equals(areTrySuccessful)) {
			this.passesTheProblemOnToTheNextChainLink(state);
		}
	}

	@Override
	public Boolean tryHandleWithProblem(ClientState state) {

		boolean successful;

		switch (state.getLastStateAlteration()) {
			case MENU_CHANGE:
				this.showMainMenu();
				state.clearLastChange();
				ClientControllerSingleton.getCONTROLLER().handlerUserOptionInput(TerminalInput.getNextLine().substring(0, 1));
				successful = true;
				break;
			case USER_OPTION_INPUT:
				char userInput = state.getLastUserInput().charAt(0);
				state.clearLastChange();
				this.handleWithUserInput(userInput);
				successful = true;
				break;
			default:
				successful = false;
		}

		return successful;

	}

	private void handleWithUserInput(char userInput) {

		ClientMainMenuOptions selectedOption = this.charToClientMainMenuOptions(userInput);

		try {
			switch (selectedOption) {
				case ADD_STOCK_TO_INTEREST_LIST:
					ClientControllerSingleton.getCONTROLLER().alterCurrentMenu(ClientMenuTypes.ADD_STOCK_TO_INTEREST_LIST);
					break;
				case STOP:
					ClientControllerSingleton.getCONTROLLER().closeTheApplication();
					break;
				case LOG_LIST:
					ClientControllerSingleton.getCONTROLLER().alterCurrentMenu(ClientMenuTypes.LOGS_LIST);
					break;
				case QUOTES:
					ClientControllerSingleton.getCONTROLLER().alterCurrentMenu(ClientMenuTypes.QUOTES_LIST);
					break;
				case INTEREST_STOCK_LIST:
					ClientControllerSingleton.getCONTROLLER().alterCurrentMenu(ClientMenuTypes.INTEREST_LIST);
					break;
				case REMOVE_STOCK_TO_INTEREST_LIST:
					ClientControllerSingleton.getCONTROLLER().alterCurrentMenu(ClientMenuTypes.REMOVE_STOCK_TO_INTEREST_LIST);
			}
		} catch (NullPointerException e) {
			this.showMainMenu();
			ClientControllerSingleton.getCONTROLLER().handlerUserOptionInput(TerminalInput.getNextLine().substring(0, 1));
		}
	}

	private ClientMainMenuOptions charToClientMainMenuOptions(char c) {

		for (ClientMainMenuOptions option : ClientMainMenuOptions.values()) {
			if (option.getOption() == c) {
				return option;
			}
		}

		return null;
	}

	private void showMainMenu() {

		String[] optionsLabel = new String[ClientMainMenuLabels.values().length];

		for (int index = 0; index < ClientMainMenuLabels.values().length; index++) {
			optionsLabel[index] = ClientMainMenuLabels.values()[index].getLabel();
		}

		System.out.println("\n\n\n");
		OutputTools.drawLine();
		OutputTools.drawOptions(optionsLabel, true);
		OutputTools.showCentralizedMessage("Client: main menu");
		OutputTools.drawLine();
	}
}

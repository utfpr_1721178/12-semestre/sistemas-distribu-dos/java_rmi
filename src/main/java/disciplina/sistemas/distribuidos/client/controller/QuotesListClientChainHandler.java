package disciplina.sistemas.distribuidos.client.controller;

import disciplina.sistemas.distribuidos.client.model.ClientChainOfResponsibility;
import disciplina.sistemas.distribuidos.client.model.ClientChainOfResponsibilityAbstractImplementation;
import disciplina.sistemas.distribuidos.client.model.ClientState;
import disciplina.sistemas.distribuidos.client.model.enums.ClientListLabel;
import disciplina.sistemas.distribuidos.client.model.enums.ClientListOption;
import disciplina.sistemas.distribuidos.client.model.enums.ClientMenuTypes;
import disciplina.sistemas.distribuidos.client.utils.ClientControllerSingleton;
import disciplina.sistemas.distribuidos.common.models.Quote;
import disciplina.sistemas.distribuidos.common.models.Stock;
import disciplina.sistemas.distribuidos.common.utils.OutputTools;
import disciplina.sistemas.distribuidos.common.utils.TerminalInput;

import java.util.List;

public class QuotesListClientChainHandler extends ClientChainOfResponsibilityAbstractImplementation {

	public QuotesListClientChainHandler(ClientChainOfResponsibility nextChain) {
		super(nextChain);
	}

	@Override
	public void receiveProblem(ClientState state) {

		Boolean areTrySuccessful = false;

		if (state.getCurrentMenu() == ClientMenuTypes.QUOTES_LIST) {
			areTrySuccessful = this.tryHandleWithProblem(state);
		}

		if (Boolean.FALSE.equals(areTrySuccessful)) {
			this.passesTheProblemOnToTheNextChainLink(state);
		}
	}

	@Override
	public Boolean tryHandleWithProblem(ClientState state) {
		boolean successful;

		switch (state.getLastStateAlteration()) {
			case USER_OPTION_INPUT:
				char userInput = state.getLastUserInput().charAt(0);
				state.clearLastChange();
				this.handleWithUserInput(userInput);
				successful = true;
				break;
			case MENU_CHANGE:
				this.showOutput();
				state.clearLastChange();
				ClientControllerSingleton.getCONTROLLER().handlerUserOptionInput(TerminalInput.getNextLine().substring(0, 1));
				successful = true;
				break;
			default:
				successful = false;
		}

		return successful;
	}

	private void handleWithUserInput(char userInput) {
		if (userInput == ClientListOption.MAIN.getOption()) {
			ClientControllerSingleton.getCONTROLLER().alterCurrentMenu(ClientMenuTypes.MAIN);
		} else {
			this.showOutput();
			ClientControllerSingleton.getCONTROLLER().handlerUserOptionInput(TerminalInput.getNextLine());
		}
	}

	private void showOutput() {

		System.out.println("\n\n\n");
		OutputTools.drawLine();
		this.showQuoteList();
		this.showOptions();
		OutputTools.showCentralizedMessage("Client: Quotes list");
		OutputTools.drawLine();
	}

	private void showQuoteList() {
		List<Quote> quotesList = ClientControllerSingleton.getCONTROLLER().getQuotesList();

		if (!quotesList.isEmpty()) {
			String[] logs = new String[quotesList.size()];

			for (int index = 0; index < logs.length; index++) {
				logs[index] = quotesList.get(index).getStock().getStockCode() + " - " +
						quotesList.get(index).getStock().getCompanyName() + " - Price: " +
						quotesList.get(index).getPrice() + "R$";
			}

			OutputTools.drawOptions(logs, false);
		} else {
			OutputTools.showCentralizedMessage("Interest stock list are empty");
			OutputTools.drawLine();
		}

	}

	private void showOptions() {
		String[] options = new String[ClientListLabel.values().length];

		for (int index = 0; index < options.length; index++) {
			options[index] = ClientListLabel.values()[index].getLabel();
		}

		OutputTools.drawOptions(options, true);
	}
}

package disciplina.sistemas.distribuidos.client.controller;

import disciplina.sistemas.distribuidos.client.model.ClientChainOfResponsibility;
import disciplina.sistemas.distribuidos.client.model.ClientChainOfResponsibilityAbstractImplementation;
import disciplina.sistemas.distribuidos.client.model.ClientState;
import disciplina.sistemas.distribuidos.client.model.enums.ClientListLabel;
import disciplina.sistemas.distribuidos.client.model.enums.ClientListOption;
import disciplina.sistemas.distribuidos.client.model.enums.ClientMenuTypes;
import disciplina.sistemas.distribuidos.client.utils.ClientControllerSingleton;
import disciplina.sistemas.distribuidos.common.utils.OutputTools;
import disciplina.sistemas.distribuidos.common.utils.TerminalInput;

import java.util.List;
import java.util.regex.Pattern;

public class LogListClientChainHandler extends ClientChainOfResponsibilityAbstractImplementation {
	private static final Pattern optionsRegexVerification = Pattern.compile("[0]");

	public LogListClientChainHandler(ClientChainOfResponsibility nextChain) {
		super(nextChain);
	}

	@Override
	public void receiveProblem(ClientState state) {

		Boolean areTrySuccessful = false;

		if (state.getCurrentMenu() == ClientMenuTypes.LOGS_LIST) {
			areTrySuccessful = this.tryHandleWithProblem(state);
		}

		if (Boolean.FALSE.equals(areTrySuccessful)) {
			this.passesTheProblemOnToTheNextChainLink(state);
		}
	}

	@Override
	public Boolean tryHandleWithProblem(ClientState state) {
		boolean successful;

		switch (state.getLastStateAlteration()) {
			case USER_OPTION_INPUT:
				char userInput = state.getLastUserInput().charAt(0);
				state.clearLastChange();
				this.handleWithUserInput(userInput, state.getLog());
				successful = true;
				break;
			case MENU_CHANGE:
				this.showOutput(state.getLog());
				state.clearLastChange();
				ClientControllerSingleton.getCONTROLLER().handlerUserOptionInput(TerminalInput.getNextLine().substring(0,1));
				successful = true;
				break;
			case NEW_LOG:
				this.showOutput(state.getLog());
				state.clearLastChange();
				successful = true;
				break;
			default:
				successful = false;
		}

		return successful;
	}

	private void handleWithUserInput(char userInput, List<String> logList) {
		if (optionsRegexVerification.matcher(String.valueOf(userInput)).find()) {
			if (userInput == ClientListOption.MAIN.getOption()) {
				ClientControllerSingleton.getCONTROLLER().alterCurrentMenu(ClientMenuTypes.MAIN);
			}
		} else {
			this.showOutput(logList);
			ClientControllerSingleton.getCONTROLLER().handlerUserOptionInput(TerminalInput.getNextLine());
		}
	}

	private void showOutput(List<String> logList) {

		System.out.println("\n\n\n");
		OutputTools.drawLine();
		this.showLogList(logList);
		this.showOptions();
		OutputTools.showCentralizedMessage("Client: LOG");
		OutputTools.drawLine();
	}

	private void showLogList(List<String> logList) {
		String[] logs = new String[logList.size()];

		for (int index = 0; index < logs.length; index++) {
			logs[index] = logList.get(index);
		}

		OutputTools.drawOptions(logs, false);
	}

	private void showOptions() {
		String[] options = new String[ClientListLabel.values().length];

		for (int index = 0; index < options.length; index++) {
			options[index] = ClientListLabel.values()[index].getLabel();
		}

		OutputTools.drawOptions(options, true);
	}
}

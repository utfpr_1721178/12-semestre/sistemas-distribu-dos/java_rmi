package disciplina.sistemas.distribuidos.client.controller;

import disciplina.sistemas.distribuidos.client.model.enums.ClientMenuTypes;
import disciplina.sistemas.distribuidos.client.utils.ClientImplementationAPI;
import disciplina.sistemas.distribuidos.common.interfaces.ClientInterfaceAPI;
import disciplina.sistemas.distribuidos.common.interfaces.ServerInterfaceAPI;
import disciplina.sistemas.distribuidos.common.models.Quote;
import disciplina.sistemas.distribuidos.common.models.Stock;
import disciplina.sistemas.distribuidos.common.utils.GlobalInformation;
import disciplina.sistemas.distribuidos.common.utils.OutputTools;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.ArrayList;
import java.util.List;

public class ClientController {

	private final Registry localDNS;

	private final ClientInterfaceAPI clientInterfaceAPI;

	private final ServerInterfaceAPI serverInterfaceAPI;

	private final ClientStateManager stateManager;

	public ClientController() {
		this.stateManager = new ClientStateManager();

		this.stateManager.addLog("Search local domain name server");
		this.localDNS = this.getLocalDomainNameServer();

		this.stateManager.addLog("DNS founded. Search server interface");
		this.serverInterfaceAPI = this.locateServerInterface();

		this.stateManager.addLog("Server interface founded. Creating own client interface");
		this.clientInterfaceAPI = this.factoryClientFactoryAPI();

		this.stateManager.addLog("Client interface created successfully. Send interface to server");
		this.registerOnTheServer();
	}

	private Registry getLocalDomainNameServer() {
		try {
			return LocateRegistry.getRegistry(
					GlobalInformation.DNS_SERVICE_ADDRESS,
					GlobalInformation.DNS_SERVICE_PORT
			);
		} catch (RemoteException e) {
			e.printStackTrace();
			OutputTools.killingServerByInternalError("FATAL ERROR: Local DNS not founded");
			return null;
		}
	}

	private ClientInterfaceAPI factoryClientFactoryAPI() {
		try {
			return new ClientImplementationAPI();
		} catch (RemoteException e) {
			e.printStackTrace();
			OutputTools.killingServerByInternalError("FATAL ERROR: Fail on create own client interface");
			return null;
		}
	}

	private ServerInterfaceAPI locateServerInterface() {
		try {
			return (ServerInterfaceAPI) localDNS.lookup(
					GlobalInformation.SERVER_NAME
			);
		} catch (RemoteException | NotBoundException e) {
			e.printStackTrace();
			OutputTools.killingServerByInternalError("FATAL ERROR: Server interface not founded");
			return null;
		}
	}

	private void registerOnTheServer() {
		try {
			this.serverInterfaceAPI.registryClient(this.clientInterfaceAPI);
		} catch (RemoteException e) {
			e.printStackTrace();
			OutputTools.killingServerByInternalError("FATAL ERROR: Register on the server failed");
		}
	}

	public void closeTheApplication() {

		System.out.println("\n\n\n\n");

		OutputTools.drawLine();
		OutputTools.showCentralizedMessage("BYE BYE");
		OutputTools.drawLine();

		System.exit(0);
	}

	public ServerInterfaceAPI getServerInterfaceAPI() {
		return serverInterfaceAPI;
	}

	public ClientInterfaceAPI getClientInterfaceAPI() {
		return clientInterfaceAPI;
	}

	public void handlerUserOptionInput(String userOptionInput) {
		this.stateManager.dealWithUserOptionInput(userOptionInput);
	}

	public void alterCurrentMenu(ClientMenuTypes nextMenu) {
		this.stateManager.dealWithAlterCurrentMenu(nextMenu);
	}

	public void createNewLog(String newLog) {
		this.stateManager.addLog(newLog);
	}

	public void addStockToInterestList(Stock stock) {
		int maxAttempts = 10;

		do {
			try {
				this.serverInterfaceAPI.addStockToInterestList(stock, this.clientInterfaceAPI);
				this.stateManager.addLog("Stock " + stock.getStockCode() + " added to interest list successfully");
				this.alterCurrentMenu(ClientMenuTypes.MAIN);
				return;
			} catch (RemoteException e) {
				this.stateManager.addLog("Fail to add stock to interest list");
				maxAttempts--;
			}
		} while (maxAttempts > 0);

		this.alterCurrentMenu(ClientMenuTypes.ADD_STOCK_TO_INTEREST_LIST);
	}

	public void removeStockToInterestList(Stock stock) {
		int maxAttempts = 10;

		do {
			try {
				this.serverInterfaceAPI.removeStockToInterestList(stock, this.clientInterfaceAPI);
				this.stateManager.addLog("Stock " + stock.getStockCode() + " remove from interest list successfully");
				this.alterCurrentMenu(ClientMenuTypes.MAIN);
				return;
			} catch (RemoteException e) {
				this.stateManager.addLog("Fail to remove stock " + stock.getStockCode() + " from interest list");
				maxAttempts--;
			}
		} while (maxAttempts > 0);

		this.alterCurrentMenu(ClientMenuTypes.REMOVE_STOCK_TO_INTEREST_LIST);
	}

	public List<Stock> getInterestList() {
		try {
			return this.serverInterfaceAPI.getInterestStocks(this.clientInterfaceAPI);
		} catch (RemoteException e) {
			this.stateManager.addLog("Fail on recuperate interests stock list from server");
			return new ArrayList<>();
		}
	}

	public List<Quote> getQuotesList() {
		try {
			return this.serverInterfaceAPI.getQuotes(this.clientInterfaceAPI);
		} catch (RemoteException e) {
			this.stateManager.addLog("Fail on recuperate quote list from server");
			return  new ArrayList<>();
		}
	}
}

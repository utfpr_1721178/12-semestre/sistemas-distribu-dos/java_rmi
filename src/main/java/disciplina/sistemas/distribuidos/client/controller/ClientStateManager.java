package disciplina.sistemas.distribuidos.client.controller;

import disciplina.sistemas.distribuidos.client.model.ClientChainOfResponsibility;
import disciplina.sistemas.distribuidos.client.model.ClientState;
import disciplina.sistemas.distribuidos.client.model.enums.ClientMenuTypes;
import disciplina.sistemas.distribuidos.client.model.enums.ClientStateAlterationsTypes;

public class ClientStateManager {

	private final ClientState state;

	private final ClientChainOfResponsibility headChainLink;

	public ClientStateManager() {
		this.state = new ClientState();

		this.headChainLink = new MainMenuClientChainHandler(null);
		this.headChainLink.addChainLinkToChain(new LogListClientChainHandler(null));
		this.headChainLink.addChainLinkToChain(new AddStockToInterestListChainHandler(null));
		this.headChainLink.addChainLinkToChain(new InterestListClientChainHandler(null));
		this.headChainLink.addChainLinkToChain(new RemoveStockToInterestListChainHandler(null));
		this.headChainLink.addChainLinkToChain(new QuotesListClientChainHandler(null));
	}

	private void dealWithStateModifications() {
		new ClientChainOfResponsibilityThread(this.headChainLink, this.state);
	}

	public void addLog(String log) {
		this.state.addLog(log);
		this.dealWithStateModifications();
	}

	public void dealWithUserOptionInput(String userOptionInput) {
		this.state.setLastUserInput(userOptionInput, ClientStateAlterationsTypes.USER_OPTION_INPUT);
		this.dealWithStateModifications();
	}

	public void dealWithAlterCurrentMenu(ClientMenuTypes nextMenu) {
		this.state.setCurrentMenu(nextMenu);
		this.dealWithStateModifications();
	}
}

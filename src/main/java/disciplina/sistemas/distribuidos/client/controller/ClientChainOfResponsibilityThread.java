package disciplina.sistemas.distribuidos.client.controller;

import disciplina.sistemas.distribuidos.client.model.ClientChainOfResponsibility;
import disciplina.sistemas.distribuidos.client.model.ClientState;

public class ClientChainOfResponsibilityThread implements Runnable {

	private final ClientChainOfResponsibility firstChainLink;

	private final ClientState state;

	public ClientChainOfResponsibilityThread(ClientChainOfResponsibility firstChainLink, ClientState state) {
		this.firstChainLink = firstChainLink;
		this.state = state;

		this.run();
	}

	@Override
	public void run() {
		if(Boolean.TRUE.equals(this.state.isDirty())) {
			this.firstChainLink.receiveProblem(this.state);
		}
	}
}


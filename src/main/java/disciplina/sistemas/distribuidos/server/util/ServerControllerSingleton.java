package disciplina.sistemas.distribuidos.server.util;

import disciplina.sistemas.distribuidos.server.controller.ServerController;

public class ServerControllerSingleton {

	private static final ServerController CONTROLLER = new ServerController();

	private ServerControllerSingleton() {
		throw new IllegalStateException("Utility class");
	}

	public static ServerController getCONTROLLER() {
		synchronized (CONTROLLER) {
			return CONTROLLER;
		}
	}
}

package disciplina.sistemas.distribuidos.server.util;

public class ServerGlobalInformation {

	public static final String[] LISTS = {
			"Return to main menu"
	};

	public static final Integer MAXIMUM_NUMBER_OF_VISIBLE_RECORDS = 10;

	private ServerGlobalInformation() {
		throw new IllegalStateException("Utility class");
	}
}

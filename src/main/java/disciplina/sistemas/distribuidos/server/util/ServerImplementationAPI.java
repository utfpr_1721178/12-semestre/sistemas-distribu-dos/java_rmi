package disciplina.sistemas.distribuidos.server.util;

import disciplina.sistemas.distribuidos.common.interfaces.ClientInterfaceAPI;
import disciplina.sistemas.distribuidos.common.interfaces.ServerInterfaceAPI;
import disciplina.sistemas.distribuidos.common.models.Quote;
import disciplina.sistemas.distribuidos.common.models.Stock;
import disciplina.sistemas.distribuidos.common.utils.OutputTools;

import java.io.Serializable;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ServerImplementationAPI extends UnicastRemoteObject implements ServerInterfaceAPI, Serializable {

	public ServerImplementationAPI() throws RemoteException {
		super();
	}

	@Override
	public void registryClient(ClientInterfaceAPI clientInterfaceAPI) throws RemoteException {
		ServerControllerSingleton.getCONTROLLER().addNewClient(clientInterfaceAPI);
	}

	@Override
	public List<Quote> getQuotes(ClientInterfaceAPI clientInterfaceAPI) {
		if (Boolean.FALSE.equals(ServerControllerSingleton.getCONTROLLER().isKnownClient(clientInterfaceAPI))) {
			return new ArrayList<>();
		}

		return ServerControllerSingleton.getCONTROLLER().getUserQuotes(clientInterfaceAPI);
	}

	@Override
	public List<Stock> getInterestStocks(ClientInterfaceAPI clientInterfaceAPI) throws RemoteException {
		return ServerControllerSingleton.getCONTROLLER().getSpecifiedClient(clientInterfaceAPI).getInterestList();
	}

	@Override
	public void addStockToInterestList(Stock stock, ClientInterfaceAPI clientInterfaceAPI) {

		ServerControllerSingleton.getCONTROLLER().addStockToInterestList(stock, clientInterfaceAPI);

	}

	@Override
	public void removeStockToInterestList(Stock stock, ClientInterfaceAPI clientInterfaceAPI) {
		ServerControllerSingleton.getCONTROLLER().removeStockToInterestList(stock,clientInterfaceAPI);
	}

	@Override
	public void buyStock(Stock stock, Integer amount, Float maximumPrice, Date deadLine, ClientInterfaceAPI clientInterfaceAPI) {
		OutputTools.showCentralizedMessage("call function: buyStock");

	}

	@Override
	public void sellStock(Stock stock, Integer amount, Float minimumPrice, Date deadLine, ClientInterfaceAPI clientInterfaceAPI) {
		OutputTools.showCentralizedMessage("call function: sellStock");

	}
}

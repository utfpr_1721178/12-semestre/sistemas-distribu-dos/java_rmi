package disciplina.sistemas.distribuidos.server.models;

import disciplina.sistemas.distribuidos.common.models.Quote;
import disciplina.sistemas.distribuidos.common.models.Stock;
import disciplina.sistemas.distribuidos.common.utils.GlobalInformation;
import disciplina.sistemas.distribuidos.common.utils.SingletonTools;
import disciplina.sistemas.distribuidos.server.models.enums.ServerMenuTypes;
import disciplina.sistemas.distribuidos.server.models.enums.ServerStateAlterationsTypes;

import java.util.*;

public class ServerState {

	private final Map<UUID, ClientProcess> knownClients;

	private final List<String> log;

	private ServerMenuTypes currentOption;

	private char lastUserInput;

	private final Queue<ServerStateAlterationsTypes> lastAlterationType;

	private final List<Quote> quotes;

	public ServerState() {

		this.knownClients = new HashMap<>();

		this.log = new ArrayList<>();

		this.currentOption = null;

		this.lastAlterationType = new LinkedList<>();

		this.lastUserInput = '@';

		this.quotes = new ArrayList<>();

		for (Stock stock : GlobalInformation.STOCKS) {
			this.quotes.add(new Quote(SingletonTools.getRandomGenerator().nextFloat() % 100 , stock));
		}
	}

	public Map<UUID, ClientProcess> getKnownClients() {
		synchronized (this.knownClients) {
			return this.knownClients;
		}
	}

	public void addKnownClients(ClientProcess newClient) {
		synchronized (this.knownClients) {
			this.knownClients.put(newClient.getUniqueUniversalIdentification(), newClient);
		}
		synchronized (this.lastAlterationType) {
			this.lastAlterationType.add(ServerStateAlterationsTypes.NEW_CLIENT);
		}
	}

	public List<String> getLog() {
		synchronized (this.log) {
			return this.log;
		}
	}

	public void addLog(String log) {
		synchronized (this.log) {
			this.log.add(log);
		}
		synchronized (this.lastAlterationType) {
			this.lastAlterationType.add(ServerStateAlterationsTypes.NEW_LOG);
		}
	}

	public synchronized ServerMenuTypes getCurrentOption() {
		return this.currentOption;
	}

	public synchronized void setCurrentOption(ServerMenuTypes currentOption) {
		this.currentOption = currentOption;

		this.lastAlterationType.add(ServerStateAlterationsTypes.MENU_CHANGE);
	}

	public synchronized char getLastUserInput() {
		return lastUserInput;
	}

	public synchronized void setLastUserInput(char lastUserInput) {
		this.lastUserInput = lastUserInput;

		this.lastAlterationType.add(ServerStateAlterationsTypes.USER_INPUT);
	}

	public Boolean isDirty() {
		synchronized (this.lastAlterationType) {
			return !this.lastAlterationType.isEmpty();
		}
	}

	public ServerStateAlterationsTypes getLastAlterationType() {
		synchronized (this.lastAlterationType) {
			return lastAlterationType.peek();
		}
	}

	public synchronized void clearLastChange() {
		this.lastAlterationType.poll();
		this.lastUserInput = '@';
	}

	public void forgetAllClients() {
		synchronized (this.knownClients) {
			this.knownClients.clear();
		}
		synchronized (this.lastAlterationType) {
			this.lastAlterationType.add(ServerStateAlterationsTypes.REMOVE_CLIENT);
		}
	}

	public List<Quote> getQuotes() {
		return quotes;
	}
}

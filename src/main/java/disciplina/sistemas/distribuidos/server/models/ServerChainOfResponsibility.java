package disciplina.sistemas.distribuidos.server.models;

public interface ServerChainOfResponsibility {

	void receiveProblem(ServerState state);

	void passesTheProblemOnToTheNextChainLink(ServerState state);

	Boolean tryHandleWithProblem(ServerState state);

	void addChainLinkToChain(ServerChainOfResponsibility chainLink);

}

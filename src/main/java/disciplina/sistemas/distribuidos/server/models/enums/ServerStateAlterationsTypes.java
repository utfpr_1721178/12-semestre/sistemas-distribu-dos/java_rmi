package disciplina.sistemas.distribuidos.server.models.enums;

public enum ServerStateAlterationsTypes {
	NEW_CLIENT, NEW_LOG, REMOVE_CLIENT, MENU_CHANGE, USER_INPUT
}

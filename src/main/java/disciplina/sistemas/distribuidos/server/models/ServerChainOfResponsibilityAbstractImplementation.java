package disciplina.sistemas.distribuidos.server.models;

public abstract class ServerChainOfResponsibilityAbstractImplementation implements ServerChainOfResponsibility {

	private ServerChainOfResponsibility nextChain;

	public ServerChainOfResponsibilityAbstractImplementation(ServerChainOfResponsibility nextChain) {
		this.nextChain = nextChain;

	}

	@Override
	public void passesTheProblemOnToTheNextChainLink(ServerState state) {
		if (this.nextChain != null) {
			this.nextChain.receiveProblem(state);
		} else {
			state.clearLastChange();
		}
	}

	@Override
	public void addChainLinkToChain(ServerChainOfResponsibility chainLink) {
		if (this.nextChain == null) {
			this.nextChain = chainLink;
		} else {
			this.nextChain.addChainLinkToChain(chainLink);
		}
	}

}

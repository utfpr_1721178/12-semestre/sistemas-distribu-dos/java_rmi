package disciplina.sistemas.distribuidos.server.models.enums;

public enum ServerListExhibitionLabels {
	MAIN("Return to main menu");

	private final String listExhibitionLabel;

	ServerListExhibitionLabels(String listExhibitionLabel) {
		this.listExhibitionLabel = listExhibitionLabel;
	}

	public String getListExhibitionLabel() {
		return this.listExhibitionLabel;
	}
}

package disciplina.sistemas.distribuidos.server.models;

import disciplina.sistemas.distribuidos.common.interfaces.ClientInterfaceAPI;
import disciplina.sistemas.distribuidos.common.models.Stock;
import disciplina.sistemas.distribuidos.common.models.StockLot;
import disciplina.sistemas.distribuidos.common.utils.GlobalInformation;
import disciplina.sistemas.distribuidos.common.utils.SingletonTools;

import java.util.*;

public class ClientProcess {

	private final ClientInterfaceAPI clientInterfaceAPI;

	private final HashMap<String, StockLot> stockLotHashMap;

	private final List<Stock> interestList;

	private final UUID uniqueUniversalIdentification;

	public ClientProcess(ClientInterfaceAPI clientInterfaceAPI, UUID id) {
		this.clientInterfaceAPI = clientInterfaceAPI;

		this.stockLotHashMap = new HashMap<>();

		this.interestList = new ArrayList<>();

		uniqueUniversalIdentification = id;

		this.generateRandomAmountOfStockLot();
	}

	private void generateRandomAmountOfStockLot() {

		int amountOfRandomStockLot = SingletonTools.getRandomGenerator().nextInt(10);

		for (int index = 0; index < amountOfRandomStockLot; index++) {
			Stock specificStock = GlobalInformation.STOCKS.get(
					SingletonTools.getRandomGenerator().nextInt(GlobalInformation.STOCKS.size())
			);

			this.buyStockLot(specificStock, (SingletonTools.getRandomGenerator().nextInt(10) + 1) * 10);
		}
	}

	public List<Stock> getInterestList() {
		return interestList;
	}

	public void buyStockLot(Stock stock, int sizeOfLot) {
		if (this.stockLotHashMap.containsKey(stock.getStockCode())) {
			StockLot stockLot = this.stockLotHashMap.get(stock.getStockCode());

			stockLot.setSizeOfLot(stockLot.getSizeOfLot() + sizeOfLot);
		} else {
			this.stockLotHashMap.put(
					stock.getStockCode(),
					new StockLot(stock, sizeOfLot)
			);

			this.addStockToInterestList(stock);
		}
	}

	public int getAmountOfStockLot() {
		return this.stockLotHashMap.size();
	}

	public ClientInterfaceAPI getClientInterfaceAPI() {
		return clientInterfaceAPI;
	}

	public UUID getUniqueUniversalIdentification() {
		return uniqueUniversalIdentification;
	}

	public void addStockToInterestList(Stock stock) {
		if(!this.interestList.contains(stock)) {
			this.interestList.add(stock);
		}
	}

	public  void removeStockToInterestList(Stock stock) {
		this.interestList.remove(stock);
	}
}

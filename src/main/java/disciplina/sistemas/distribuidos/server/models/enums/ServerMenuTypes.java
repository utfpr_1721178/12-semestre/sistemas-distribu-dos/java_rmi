package disciplina.sistemas.distribuidos.server.models.enums;

public enum ServerMenuTypes {
	MAIN, LOGS, CLIENT_LIST
}

package disciplina.sistemas.distribuidos.server.models.enums;

public enum ServerMainMenuOptions {
	CLIENTS_LIST('0'),
	LOG_LIST('1'),
	STOP_SERVER('2');

	private final char menuOption;

	ServerMainMenuOptions(char menuOption) {
		this.menuOption = menuOption;
	}

	public char getMenuOption() {
		return this.menuOption;
	}
}

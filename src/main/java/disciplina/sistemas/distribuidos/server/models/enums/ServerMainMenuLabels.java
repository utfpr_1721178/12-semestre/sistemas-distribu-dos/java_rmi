package disciplina.sistemas.distribuidos.server.models.enums;

public enum ServerMainMenuLabels {
	CLIENTS_LIST("See list of known clients"),
	LOG_LIST("See process log"),
	STOP_SERVER("Stop server");

	private final String menuLabel;

	ServerMainMenuLabels(String menuLabel) {
		this.menuLabel = menuLabel;
	}

	public String getMenuLabel() {
		return this.menuLabel;
	}
}



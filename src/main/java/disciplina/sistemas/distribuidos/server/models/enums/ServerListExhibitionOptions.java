package disciplina.sistemas.distribuidos.server.models.enums;

public enum ServerListExhibitionOptions {
	MAIN('0');

	private final char listExhibitionOption;

	ServerListExhibitionOptions(char listExhibitionOption) {
		this.listExhibitionOption = listExhibitionOption;
	}

	public char getListExhibitionOption() {
		return this.listExhibitionOption;
	}
}

package disciplina.sistemas.distribuidos.server.controller;

import disciplina.sistemas.distribuidos.common.utils.OutputTools;
import disciplina.sistemas.distribuidos.common.utils.TerminalInput;
import disciplina.sistemas.distribuidos.server.models.ServerChainOfResponsibility;
import disciplina.sistemas.distribuidos.server.models.ServerChainOfResponsibilityAbstractImplementation;
import disciplina.sistemas.distribuidos.server.models.ServerState;
import disciplina.sistemas.distribuidos.server.models.enums.ServerListExhibitionLabels;
import disciplina.sistemas.distribuidos.server.models.enums.ServerListExhibitionOptions;
import disciplina.sistemas.distribuidos.server.models.enums.ServerMenuTypes;
import disciplina.sistemas.distribuidos.server.util.ServerControllerSingleton;

import java.util.List;
import java.util.regex.Pattern;

public class LogListServerChainHandler extends ServerChainOfResponsibilityAbstractImplementation {
	private static final Pattern optionsRegexVerification = Pattern.compile("[0]");

	public LogListServerChainHandler(ServerChainOfResponsibility nextChain) {
		super(nextChain);
	}

	@Override
	public void receiveProblem(ServerState state) {

		Boolean areTrySuccessful = false;

		if (state.getCurrentOption() == ServerMenuTypes.LOGS) {
			areTrySuccessful = this.tryHandleWithProblem(state);
		}

		if (Boolean.FALSE.equals(areTrySuccessful)) {
			this.passesTheProblemOnToTheNextChainLink(state);
		}
	}

	@Override
	public Boolean tryHandleWithProblem(ServerState state) {
		boolean successful;

		switch (state.getLastAlterationType()) {
			case USER_INPUT:
				char userInput = state.getLastUserInput();
				state.clearLastChange();
				this.handleWithUserInput(userInput, state.getLog());
				successful = true;
				break;
			case MENU_CHANGE:
				this.showOutput(state.getLog());
				state.clearLastChange();
				ServerControllerSingleton.getCONTROLLER().handlerUserInput(TerminalInput.getNextLine().charAt(0));
				successful = true;
				break;
			case NEW_LOG:
				this.showOutput(state.getLog());
				state.clearLastChange();
				successful = true;
				break;
			default:
				successful = false;
		}

		return successful;
	}

	private void handleWithUserInput(char userInput, List<String> logList) {
		if (optionsRegexVerification.matcher(String.valueOf(userInput)).find()) {
			if (userInput == ServerListExhibitionOptions.MAIN.getListExhibitionOption()) {
				ServerControllerSingleton.getCONTROLLER().alterCurrentMenu(ServerMenuTypes.MAIN);
			}
		} else {
			this.showOutput(logList);
			ServerControllerSingleton.getCONTROLLER().handlerUserInput(TerminalInput.getNextLine().charAt(0));
		}
	}

	private void showOutput(List<String> logList) {

		System.out.println("\n\n\n");
		OutputTools.drawLine();
		this.showLogList(logList);
		this.showOptions();
		OutputTools.showCentralizedMessage("Client: LOG");
		OutputTools.drawLine();
	}

	private void showLogList(List<String> logList) {
		String[] logs = new String[logList.size()];

		for (int index = 0; index < logs.length; index++) {
			logs[index] = logList.get(index);
		}

		OutputTools.drawOptions(logs, false);
	}

	private void showOptions() {
		String[] options = new String[ServerListExhibitionLabels.values().length];

		for (int index = 0; index < options.length; index++) {
			options[index] = ServerListExhibitionLabels.values()[index].getListExhibitionLabel();
		}

		OutputTools.drawOptions(options, true);
	}
}

package disciplina.sistemas.distribuidos.server.controller;

import disciplina.sistemas.distribuidos.server.models.ServerChainOfResponsibility;
import disciplina.sistemas.distribuidos.server.models.ServerState;

public class ServerChainOfResponsibilityThread implements Runnable {

	private final ServerChainOfResponsibility firstChainLink;

	private final ServerState state;

	public ServerChainOfResponsibilityThread(ServerChainOfResponsibility firstChainLink, ServerState state) {
		this.firstChainLink = firstChainLink;
		this.state = state;

		this.run();
	}

	@Override
	public void run() {
		if(Boolean.TRUE.equals(this.state.isDirty())) {
			this.firstChainLink.receiveProblem(this.state);
		}
	}
}


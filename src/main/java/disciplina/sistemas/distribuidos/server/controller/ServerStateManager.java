package disciplina.sistemas.distribuidos.server.controller;

import disciplina.sistemas.distribuidos.common.interfaces.ClientInterfaceAPI;
import disciplina.sistemas.distribuidos.common.models.Quote;
import disciplina.sistemas.distribuidos.common.models.Stock;
import disciplina.sistemas.distribuidos.server.models.ServerChainOfResponsibility;
import disciplina.sistemas.distribuidos.server.models.ClientProcess;
import disciplina.sistemas.distribuidos.server.models.ServerState;
import disciplina.sistemas.distribuidos.server.models.enums.ServerMenuTypes;

import java.rmi.RemoteException;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

public class ServerStateManager {

	private final ServerState state;

	private final ServerChainOfResponsibility firstChainLink;

	public ServerStateManager() {

		this.state = new ServerState();

		this.firstChainLink = new MainMenuServerChainHandler(null);
		this.firstChainLink.addChainLinkToChain(new ClientListServerChainHandler(null));
		this.firstChainLink.addChainLinkToChain(new LogListServerChainHandler(null));

	}

	public void addNewLog(String message) {
		this.state.addLog(message);
		this.dealWithStateChanges();
	}

	public void killAllClients() {
		Map<UUID, ClientProcess> clientList = this.state.getKnownClients();

		this.state.forgetAllClients();
		this.dealWithStateChanges();

		for (ClientProcess client : clientList.values()) {
			try {
				client.getClientInterfaceAPI().killHomeBroker();
			} catch (RemoteException e) {
				e.printStackTrace();
			}
		}
	}

	public void alterCurrentMenu(ServerMenuTypes menuType) {
		this.state.setCurrentOption(menuType);
		this.dealWithStateChanges();
	}

	public void handlerUserInput(char userInput) {
		this.state.setLastUserInput(userInput);
		this.dealWithStateChanges();
	}

	public void addNewClient(ClientInterfaceAPI clientInterfaceAPI) {
		UUID id = this.getUUIFromClientInterface(clientInterfaceAPI);

		if (id == null) {
			this.state.addLog("Fail to try add client");
		} else if (this.state.getKnownClients().containsKey(id)) {
			this.state.addLog("Fail to try add client: Client ID " + id + " already exists");
		} else {
			this.state.addKnownClients(new ClientProcess(clientInterfaceAPI, id));
			this.dealWithStateChanges();
			this.state.addLog("Add client successfully: Client ID " + id);
		}

		this.dealWithStateChanges();
	}

	public Boolean isKnowClient(ClientInterfaceAPI clientInterfaceAPI) {
		UUID id = this.getUUIFromClientInterface(clientInterfaceAPI);

		if (id == null) {
			return false;
		} else {
			return this.state.getKnownClients().containsKey(id);
		}
	}

	public ClientProcess getSpecifiedClient(ClientInterfaceAPI clientInterfaceAPI) {
		UUID id = this.getUUIFromClientInterface(clientInterfaceAPI);

		return id == null ? null : this.state.getKnownClients().get(id);
	}

	private UUID getUUIFromClientInterface(ClientInterfaceAPI clientInterfaceAPI) {

		try {
			return clientInterfaceAPI.getUniversallyUniqueIdentifier();
		} catch (RemoteException e) {
			this.state.addLog("Fail on get UUID from client interface");
			this.dealWithStateChanges();
		}

		return null;
	}

	public void addStockToInterestList(Stock stock, ClientInterfaceAPI clientInterfaceAPI) {
		ClientProcess clientProcess = this.getSpecifiedClient(clientInterfaceAPI);

		if (clientProcess != null) {
			clientProcess.addStockToInterestList(stock);
			this.state.addLog("Add " + stock.getStockCode() + " to interest list from client " +
					clientProcess.getUniqueUniversalIdentification().toString());
		} else {
			this.state.addLog("Fail to add stock to interest list: Client not known");
		}

		this.dealWithStateChanges();
	}

	public void removeStockToInterestList(Stock stock, ClientInterfaceAPI clientInterfaceAPI) {
		ClientProcess clientProcess = this.getSpecifiedClient(clientInterfaceAPI);

		if (clientProcess != null) {
			clientProcess.removeStockToInterestList(stock);
			this.state.addLog("Remove " + stock.getStockCode() + " to interest list from client " +
					clientProcess.getUniqueUniversalIdentification().toString());
		} else {
			this.state.addLog("Fail to remove stock to interest list: Client not known");
		}

		this.dealWithStateChanges();
	}

	public List<Quote> getUserQuotes(ClientInterfaceAPI clientInterfaceAPI) {
		ClientProcess clientProcess = this.getSpecifiedClient(clientInterfaceAPI);

		return this.state.getQuotes().stream().filter(
				quote -> clientProcess != null && clientProcess.getInterestList().contains(quote.getStock())
		).collect(Collectors.toList());

	}

	private void dealWithStateChanges() {
		new ServerChainOfResponsibilityThread(this.firstChainLink, this.state);
	}

}

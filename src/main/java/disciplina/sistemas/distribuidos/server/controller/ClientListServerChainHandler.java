package disciplina.sistemas.distribuidos.server.controller;

import disciplina.sistemas.distribuidos.common.utils.OutputTools;
import disciplina.sistemas.distribuidos.common.utils.TerminalInput;
import disciplina.sistemas.distribuidos.server.models.ServerChainOfResponsibility;
import disciplina.sistemas.distribuidos.server.models.ServerChainOfResponsibilityAbstractImplementation;
import disciplina.sistemas.distribuidos.server.models.ClientProcess;
import disciplina.sistemas.distribuidos.server.models.ServerState;
import disciplina.sistemas.distribuidos.server.models.enums.ServerListExhibitionLabels;
import disciplina.sistemas.distribuidos.server.models.enums.ServerListExhibitionOptions;
import disciplina.sistemas.distribuidos.server.models.enums.ServerMenuTypes;
import disciplina.sistemas.distribuidos.server.util.ServerControllerSingleton;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

public class ClientListServerChainHandler extends ServerChainOfResponsibilityAbstractImplementation {

	private static final Pattern optionsRegexVerification = Pattern.compile("[0]");

	public ClientListServerChainHandler(ServerChainOfResponsibility nextChain) {
		super(nextChain);
	}

	@Override
	public void receiveProblem(ServerState state) {

		boolean areTrySuccessful = false;

		if (state.getCurrentOption() == ServerMenuTypes.CLIENT_LIST) {
			areTrySuccessful = this.tryHandleWithProblem(state);
		}

		if (Boolean.FALSE.equals(areTrySuccessful)) {
			this.passesTheProblemOnToTheNextChainLink(state);
		}
	}

	@Override
	public Boolean tryHandleWithProblem(ServerState state) {
		boolean successful;

		switch (state.getLastAlterationType()) {
			case MENU_CHANGE:
				this.showOutput(new ArrayList<>(state.getKnownClients().values()));
				state.clearLastChange();
				ServerControllerSingleton.getCONTROLLER().handlerUserInput(TerminalInput.getNextLine().charAt(0));
				successful = true;
				break;
			case USER_INPUT:
				char userInput = state.getLastUserInput();
				state.clearLastChange();
				this.handleWithUserInput(userInput, new ArrayList<>(state.getKnownClients().values()));
				successful = true;
				break;
			case NEW_CLIENT:
			case REMOVE_CLIENT:
				this.showOutput(new ArrayList<>(state.getKnownClients().values()));
				state.clearLastChange();
				successful = true;
				break;
			default:
				successful = false;
		}

		return successful;
	}

	private void handleWithUserInput(char c, List<ClientProcess> clientProcessList) {
		if (optionsRegexVerification.matcher(String.valueOf(c)).find()) {
			if (c == ServerListExhibitionOptions.MAIN.getListExhibitionOption()) {
				ServerControllerSingleton.getCONTROLLER().alterCurrentMenu(ServerMenuTypes.MAIN);
			}

		} else {
			this.showOutput(clientProcessList);
			ServerControllerSingleton.getCONTROLLER().handlerUserInput(TerminalInput.getNextLine().charAt(0));
		}
	}

	private void showOutput(List<ClientProcess> clientProcessList) {

		System.out.println("\n\n\n");
		OutputTools.drawLine();
		this.showKnownClientList(clientProcessList);
		this.showOptions();
		OutputTools.showCentralizedMessage("Server: Known client list");
		OutputTools.drawLine();
	}

	private void showKnownClientList(List<ClientProcess> clientProcessList) {

		if (clientProcessList.isEmpty()) {
			OutputTools.showCentralizedMessage("No known client");
			OutputTools.drawLine();
		} else {
			String[] clientPresentationLabels = new String[clientProcessList.size()];

			for (int index = 0; index < clientPresentationLabels.length; index++) {
				clientPresentationLabels[index] = "Client identified with ID: " + clientProcessList.get(index).hashCode();
			}

			OutputTools.drawOptions(clientPresentationLabels, false);
		}
	}

	private void showOptions() {
		String[] options = new String[ServerListExhibitionLabels.values().length];

		for (int index = 0; index < options.length; index++) {
			options[index] = ServerListExhibitionLabels.values()[index].getListExhibitionLabel();
		}

		OutputTools.drawOptions(options, true);
	}

}

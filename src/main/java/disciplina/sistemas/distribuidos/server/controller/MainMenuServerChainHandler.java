package disciplina.sistemas.distribuidos.server.controller;

import disciplina.sistemas.distribuidos.common.utils.OutputTools;
import disciplina.sistemas.distribuidos.common.utils.TerminalInput;
import disciplina.sistemas.distribuidos.server.models.ServerChainOfResponsibility;
import disciplina.sistemas.distribuidos.server.models.ServerChainOfResponsibilityAbstractImplementation;
import disciplina.sistemas.distribuidos.server.models.ServerState;
import disciplina.sistemas.distribuidos.server.models.enums.ServerMainMenuLabels;
import disciplina.sistemas.distribuidos.server.models.enums.ServerMainMenuOptions;
import disciplina.sistemas.distribuidos.server.models.enums.ServerMenuTypes;
import disciplina.sistemas.distribuidos.server.util.ServerControllerSingleton;

import java.util.regex.Pattern;

public class MainMenuServerChainHandler extends ServerChainOfResponsibilityAbstractImplementation {

	private static final Pattern optionsRegexVerification = Pattern.compile("[0-" + (ServerMainMenuLabels.values().length - 1 + '0') + "]");

	public MainMenuServerChainHandler(ServerChainOfResponsibility nextChain) {
		super(nextChain);
	}

	@Override
	public void
	receiveProblem(ServerState state) {

		boolean areTrySuccessful = false;

		if (state.getCurrentOption() == ServerMenuTypes.MAIN) {
			areTrySuccessful = this.tryHandleWithProblem(state);
		}

		if (Boolean.FALSE.equals(areTrySuccessful)) {
			this.passesTheProblemOnToTheNextChainLink(state);
		}
	}

	@Override
	public Boolean tryHandleWithProblem(ServerState state) {

		boolean successful;

		switch (state.getLastAlterationType()) {
			case MENU_CHANGE:
				this.showMainMenu();
				state.clearLastChange();
				ServerControllerSingleton.getCONTROLLER().handlerUserInput(TerminalInput.getNextLine().charAt(0));
				successful = true;
				break;
			case USER_INPUT:
				char userInput = state.getLastUserInput();
				state.clearLastChange();
				this.handleWithUserInput(userInput);
				successful = true;
				break;
			default:
				successful = false;
		}

		return successful;

	}

	private void handleWithUserInput(char userInput) {

		if (optionsRegexVerification.matcher(String.valueOf(userInput)).find()) {

			if (ServerMainMenuOptions.LOG_LIST.getMenuOption() == userInput) {
				ServerControllerSingleton.getCONTROLLER().alterCurrentMenu(ServerMenuTypes.LOGS);
			} else if (ServerMainMenuOptions.CLIENTS_LIST.getMenuOption() == userInput) {
				ServerControllerSingleton.getCONTROLLER().alterCurrentMenu(ServerMenuTypes.CLIENT_LIST);
			} else {
				ServerControllerSingleton.getCONTROLLER().closeTheApplication();
			}
		} else {
			this.showMainMenu();
			ServerControllerSingleton.getCONTROLLER().handlerUserInput(TerminalInput.getNextLine().charAt(0));
		}
	}

	private void showMainMenu() {

		String[] optionsLabel = new String[ServerMainMenuLabels.values().length];

		for (int index = 0; index < ServerMainMenuLabels.values().length; index++) {
			optionsLabel[index] = ServerMainMenuLabels.values()[index].getMenuLabel();
		}

		System.out.println("\n\n\n");
		OutputTools.drawLine();
		OutputTools.drawOptions(optionsLabel, true);
		OutputTools.showCentralizedMessage("Server: main menu");
		OutputTools.drawLine();
	}
}

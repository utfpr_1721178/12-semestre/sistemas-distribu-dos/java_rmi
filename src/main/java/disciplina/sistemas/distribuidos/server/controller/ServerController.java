package disciplina.sistemas.distribuidos.server.controller;

import disciplina.sistemas.distribuidos.common.interfaces.ClientInterfaceAPI;
import disciplina.sistemas.distribuidos.common.interfaces.ServerInterfaceAPI;
import disciplina.sistemas.distribuidos.common.models.Quote;
import disciplina.sistemas.distribuidos.common.models.Stock;
import disciplina.sistemas.distribuidos.common.utils.GlobalInformation;
import disciplina.sistemas.distribuidos.common.utils.OutputTools;
import disciplina.sistemas.distribuidos.server.models.ClientProcess;
import disciplina.sistemas.distribuidos.server.models.enums.ServerMenuTypes;
import disciplina.sistemas.distribuidos.server.util.ServerImplementationAPI;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.List;

public class ServerController {

	private Registry localDNS;

	private final ServerStateManager stateManager;

	private ServerInterfaceAPI serverInterfaceAPI;

	public ServerController() {
		this.stateManager = new ServerStateManager();

		this.createLocalDNS();

		this.createOwnServerInterface();

		this.oferServerInterfaceOnLocalDNS();
	}

	private void createLocalDNS() {
		this.stateManager.addNewLog("Searching for local domain name server");

		int maxAttempts = 10;

		do {
			try {
				this.localDNS = LocateRegistry.createRegistry(GlobalInformation.DNS_SERVICE_PORT);
				this.stateManager.addNewLog("DNS founded");
				return;
			} catch (RemoteException e) {
				this.stateManager.addNewLog("Fail on searching for local domain name server");
				maxAttempts--;
			}
		} while (maxAttempts > 0);

		this.stateManager.addNewLog("FATAL ERROR: Local DNS not founded");

		OutputTools.killingServerByInternalError("FATAL ERROR: Local DNS not founded");
		OutputTools.drawLine();
	}

	private void createOwnServerInterface() {
		this.stateManager.addNewLog("Creating own interface");

		int maxAttempts = 10;

		do {
			try {
				this.serverInterfaceAPI = new ServerImplementationAPI();
				this.stateManager.addNewLog("Server interface create successfully");
				return;
			} catch (RemoteException e) {
				this.stateManager.addNewLog("Fail on creating own interface");
				maxAttempts--;
			}
		} while (maxAttempts > 0);

		this.stateManager.addNewLog("FATAL ERROR: Fail on creating server interface");
		OutputTools.killingServerByInternalError("FATAL ERROR: Fail on creating server interface");

	}

	private void oferServerInterfaceOnLocalDNS() {

		this.stateManager.addNewLog("Linking server interface with DNS name");

		int maxAttempts = 10;

		do {
			try {
				this.localDNS.rebind(GlobalInformation.SERVER_NAME, serverInterfaceAPI);
				this.stateManager.addNewLog("Server interface successfully offered");
				return;

			} catch (RemoteException e) {
				this.stateManager.addNewLog("Attempt to offer server interface failed");
				maxAttempts--;
			}
		} while (maxAttempts > 0);

		this.stateManager.addNewLog("FATAL ERROR: Offering the server interface failed");
		OutputTools.killingServerByInternalError("FATAL ERROR: Offering the server interface failed");
	}
	
	public void closeTheApplication() {

		this.stateManager.killAllClients();

		try {
			this.localDNS.unbind(GlobalInformation.SERVER_NAME);
		} catch (RemoteException | NotBoundException e) {
			e.printStackTrace();
		}

		System.out.println("\n\n\n\n");

		OutputTools.drawLine();
		OutputTools.showCentralizedMessage("BYE BYE");
		OutputTools.drawLine();

		System.exit(0);
	}

	public void handlerUserInput(char userInput) {
		this.stateManager.handlerUserInput(userInput);
	}

	public void alterCurrentMenu(ServerMenuTypes menuType) {
		this.stateManager.alterCurrentMenu(menuType);
	}

	public void addNewClient(ClientInterfaceAPI clientInterfaceAPI) {
		this.stateManager.addNewClient(clientInterfaceAPI);
	}

	public Boolean isKnownClient(ClientInterfaceAPI clientInterfaceAPI) {
		return this.stateManager.isKnowClient(clientInterfaceAPI);
	}

	public ClientProcess getSpecifiedClient(ClientInterfaceAPI clientInterfaceAPI) {
		if (Boolean.TRUE.equals(this.stateManager.isKnowClient(clientInterfaceAPI))) {
			return this.stateManager.getSpecifiedClient(clientInterfaceAPI);
		} else {
			return null;
		}
	}

	public void addStockToInterestList(Stock stock, ClientInterfaceAPI clientInterfaceAPI) {
		this.stateManager.addStockToInterestList(stock, clientInterfaceAPI);
	}

	public void removeStockToInterestList(Stock stock, ClientInterfaceAPI clientInterfaceAPI) {
		this.stateManager.removeStockToInterestList(stock,clientInterfaceAPI);
	}

	public List<Quote> getUserQuotes(ClientInterfaceAPI clientInterfaceAPI) {
		return this.stateManager.getUserQuotes(clientInterfaceAPI);
	}
}
